# HarvardX-CS50AI

This repository contains the results of the HavardX course [CS50's Introduction to Artificial Intelligence with Python](https://pll.harvard.edu/course/cs50s-introduction-artificial-intelligence-python)).

The script and videos are found [here](https://cs50.harvard.edu/ai/2024/).

Overview:

- Course 1: Search algorithms
- Course 2: Knowledge-based algorithms

All code is written in `python3`. To set up the environment use these steps:

```bash
sudo apt install python3.10-venv

# set up the virtual environment
python3 -m venv ./venv

# activate the venv
source ./venv/bin/activate

# install needed libraries
pip install -r requirements.txt
```

## Run the course scripts

To run a script from one of the courses run

```bash
# make sure you activated the venv first

cd course-x
main.py  # shows parameter options
```

Additional parameters might be needed that you can check by appending `-h`.

Examples for each script:

```bash
cd course-1-search
./main.py ./mazes/maze2.txt queue  # breadth search
./main.py ./mazes/maze2.txt stack  # depth search
# other mazes are found in the `mazes` subfolder

cd course-2-knowledge
./main.py harry  # simple model-based learning
./main.py clue  # clue game solver
./main.py mastermind  # mastermind game solving
./main.py harrypuzzle  # figure out which character lives where
```

**To be continued soon ...**
