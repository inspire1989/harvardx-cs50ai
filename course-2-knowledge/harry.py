from logic import *


def harry():
    rain = Symbol("rain")  # it's raining
    hagrid = Symbol("hagrid")  # Harry visits Hagrid
    dumbledore = Symbol("dumbledore")  # Harry visits Dumbledore

    knowledge = And(
        Implication(Not(rain), hagrid),  # if it's not raining, Harry visits Hagrid
        Or(hagrid, dumbledore),  # Harry visits either Hagrid or Dumbledore
        Not(And(hagrid, dumbledore)),  # Harry doesn't visit both Hagrid and Dumbledore
        dumbledore,  # Hagrid visits Dumbledore
    )
    print(f"Formula: {knowledge.formula()}")

    print()

    print(
        f"Based on my knowledge, did it rain? Answer: {model_check(knowledge, Symbol('rain'))}"
    )

    print(
        f"Based on my knowledge, did Harry visit Hagrid? Answer: {model_check(knowledge, Symbol('hagrid'))}"
    )
