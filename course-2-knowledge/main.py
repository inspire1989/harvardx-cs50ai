#! /usr/bin/env python3

import argparse
from clue import *
from harry import *
from harrypuzzle import *
from mastermind import *


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "puzzle",
        help="Puzzle to solve.",
        choices=["harry", "clue", "harrypuzzle", "mastermind"],
    )
    args = parser.parse_args()

    if args.puzzle == "harry":
        harry()
    elif args.puzzle == "clue":
        clue()
    elif args.puzzle == "harrypuzzle":
        harrypuzzle()
    elif args.puzzle == "mastermind":
        mastermind()
    else:
        raise Exception("Unsupported puzzle")


if __name__ == "__main__":
    main()
