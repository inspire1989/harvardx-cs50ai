class FrontierBase:
    """Base class for all frontiers"""

    def __init__(self):
        self.frontier = []

    def add(self, node):
        self.frontier.append(node)

    def contains_state(self, state):
        return any(node.state == state for node in self.frontier)

    def empty(self):
        return len(self.frontier) == 0


class StackFrontier(FrontierBase):
    """LIFO frontier implementation"""

    def remove(self):
        if self.empty():
            raise Exception("empty frontier")
        else:
            # remove the last element and return it
            return self.frontier.pop()


class QueueFrontier(FrontierBase):
    """FIFO frontier implementation"""

    def remove(self):
        if self.empty():
            raise Exception("empty frontier")
        else:
            # remove the first element and return it
            return self.frontier.pop(0)
