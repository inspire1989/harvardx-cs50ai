#! /usr/bin/env python3

import argparse

from Maze import *
from Frontier import *


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "maze", help='The maze text file to use. E.g. "mazes/maze1.txt".'
    )
    parser.add_argument(
        "frontier", help="The frontier to use.", choices=["stack", "queue"]
    )
    args = parser.parse_args()

    if args.frontier == "stack":
        frontier = StackFrontier()
    elif args.frontier == "queue":
        frontier = QueueFrontier()
    else:
        raise Exception("Unsupported frontier")

    maze = Maze(args.maze)

    print("Maze before solving:")
    maze.print()

    print("Solving ...")
    maze.solve(frontier)

    print(f"States explored: {maze.num_explored}")
    print("Solution:")
    maze.print()

    maze.output_image(
        f"{os.path.dirname(os.path.realpath(__file__))}/solution.png", True, True
    )


if __name__ == "__main__":
    main()
